package com.example.weather_app_capstone.presentation.view

import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.weather_app_capstone.R
import com.example.weather_app_capstone.data.datasource.local.WeatherDB
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScheduler
import kotlinx.coroutines.test.TestScope
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LocationsFragmentTest {
    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkRecyclerViewWorking() {
        activityScenarioRule.scenario.onActivity { activity ->
            val navHostFragment =
                activity.supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
            val navController = navHostFragment.navController
            navController.navigate(R.id.action_homeFragment_to_locationsFragment)
        }
        Espresso.onView(allOf(ViewMatchers.withId(R.id.category_name_tv), withText("Main")))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun checkSavedLocationAppearingInList() {
        activityScenarioRule.scenario.onActivity { activity ->
            val weatherDao = WeatherDB.getDatabase(activity).weatherDao()
            CoroutineScope(Dispatchers.IO).launch {
                weatherDao.clearDailyForecastTable()
            }
            activity.viewModel.deleteLocation()
        }
        Thread.sleep(1000)
        Espresso.onView(ViewMatchers.withId(R.id.saveLocationIV)).perform(ViewActions.click())
        activityScenarioRule.scenario.onActivity { activity ->
            val navHostFragment =
                activity.supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
            val navController = navHostFragment.navController
            navController.navigate(R.id.action_homeFragment_to_locationsFragment)
        }

        Espresso.onView(ViewMatchers.withId(R.id.recycler_view))
            .perform(
                RecyclerViewActions.scrollTo<RecyclerView.ViewHolder>(
                    ViewMatchers.hasDescendant(withText(CITY_TO_TEST))
                )
            )
            .check(
                ViewAssertions.matches(
                    ViewMatchers.hasDescendant(withText(CITY_TO_TEST))
                )
            )
    }

}