package com.example.weather_app_capstone.presentation.view

import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.weather_app_capstone.R
import kotlinx.coroutines.delay
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

const val CITY_TO_TEST = "Nur-Sultan"

@RunWith(AndroidJUnit4::class)
class HomeFragmentTest {
    @get:Rule
    val activityScenarioRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkHomeFragmentShowingDataFromRest() {
        Thread.sleep(1000)
        Espresso.onView(withId(R.id.locationTV))
            .check(ViewAssertions.matches(withText(CITY_TO_TEST)))
    }


}