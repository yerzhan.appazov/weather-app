package com.example.weather_app_capstone.domain.mapper

import com.example.weather_app_capstone.common.ICON_BASE_URL
import com.example.weather_app_capstone.domain.model.HomeWeatherDomainModel
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel
import javax.inject.Inject

class LocationsWeatherMapper @Inject constructor() {
    fun mapCurrentWeatherToLocations(currentWeather: HomeWeatherDomainModel) =
        SavedLocationsWeatherDomainModel(
            currentWeather.name,
            currentWeather.description,
            currentWeather.temp,
            currentWeather.highest,
            currentWeather.lowest,
            currentWeather.icon,
            currentWeather.latitude,
            currentWeather.longitude,
            currentWeather.cityId
        )
}