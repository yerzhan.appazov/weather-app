package com.example.weather_app_capstone.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.weather_app_capstone.common.CURRENT_WEATHER_TABLE

@Entity(CURRENT_WEATHER_TABLE)
data class HomeWeatherDomainModel(
    val name: String,
    val description: String,
    val temp: Double,
    val highest: Double,
    val lowest: Double,
    val icon: String,
    val latitude: String,
    val longitude: String,
    @PrimaryKey
    val cityId: Int
)