package com.example.weather_app_capstone.domain.usecase

import com.example.weather_app_capstone.data.repository.WeatherRepository
import com.example.weather_app_capstone.domain.mapper.DailyForecastMapper
import javax.inject.Inject

class GetDailyForecastRemoteUseCase @Inject constructor(
    private val mapper: DailyForecastMapper,
    private val repository: WeatherRepository
) {
    suspend fun execute(latitude: String, longitude: String, token: String, units: String) =
        repository.getDailyForecastRemote(latitude, longitude, token, units)
            ?.let { mapper.mapDataDailyForecastToDomain(it) }
}