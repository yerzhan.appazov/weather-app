package com.example.weather_app_capstone.domain.usecase

import com.example.weather_app_capstone.data.repository.WeatherRepository
import javax.inject.Inject

class GetLocationsWeatherLocalUseCase @Inject constructor(
    private val repository: WeatherRepository,
) {
    suspend fun execute() = repository.getSavedLocationsLocal()
}