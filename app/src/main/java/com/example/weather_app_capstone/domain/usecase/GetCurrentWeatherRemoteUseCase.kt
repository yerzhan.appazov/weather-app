package com.example.weather_app_capstone.domain.usecase

import android.util.Log
import com.example.weather_app_capstone.data.repository.WeatherRepository
import com.example.weather_app_capstone.domain.mapper.CurrentWeatherMapper
import javax.inject.Inject

class GetCurrentWeatherRemoteUseCase @Inject constructor(
    private val mapper: CurrentWeatherMapper,
    private val repository: WeatherRepository
) {
    suspend fun execute(latitude: String, longitude: String, token: String, units: String) =
        repository.getCurrentWeatherRemote(latitude, longitude, token, units)
            ?.let {
                mapper.mapCurrentWeatherToDomainModel(it)
            }
}