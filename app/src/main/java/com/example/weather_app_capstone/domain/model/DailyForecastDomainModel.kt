package com.example.weather_app_capstone.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.weather_app_capstone.common.DAILY_FORECAST_TABLE

@Entity(DAILY_FORECAST_TABLE)
data class DailyForecastDomainModel(
    val time: String,
    val description: String,
    val temp: Double,
    val ico: String,
    @PrimaryKey(autoGenerate = true)
    val dbId: Int = 0,
)