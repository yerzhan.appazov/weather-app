package com.example.weather_app_capstone.domain.mapper

import com.example.weather_app_capstone.common.ICON_BASE_URL
import com.example.weather_app_capstone.data.model.CurrentWeatherDataModel
import com.example.weather_app_capstone.domain.model.HomeWeatherDomainModel
import javax.inject.Inject

class CurrentWeatherMapper @Inject constructor() {
    fun mapCurrentWeatherToDomainModel(currentWeather: CurrentWeatherDataModel) =
        HomeWeatherDomainModel(
            currentWeather.name,
            currentWeather.weather[0].description,
            currentWeather.main.temp,
            currentWeather.main.temp_max,
            currentWeather.main.temp_min,
            "$ICON_BASE_URL${currentWeather.weather[0].icon}.png",
            currentWeather.coord.lat.toString(),
            currentWeather.coord.lon.toString(),
            currentWeather.id
        )
}