package com.example.weather_app_capstone.domain.mapper

import com.example.weather_app_capstone.common.TIME_AFTER_NOON
import com.example.weather_app_capstone.data.model.DailyForecastDataModel
import com.example.weather_app_capstone.domain.model.DailyForecastDomainModel
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale
import javax.inject.Inject

class DailyForecastMapper @Inject constructor() {
    fun mapDataDailyForecastToDomain(dailyForecast: DailyForecastDataModel) =
        dailyForecast.list
            .filter { it.dt_txt.contains(TIME_AFTER_NOON) }
            .map {
                DailyForecastDomainModel(
                it.dt_txt.substring(0, 10).getWeekDayName(),
                it.weather[0].description,
                it.main.temp,
                it.weather[0].icon
                )
            }
            .toList()

    private fun String.getWeekDayName(): String {
        val dtfInput = DateTimeFormatter.ofPattern("u-M-d", Locale.ENGLISH)
        val dtfOutput = DateTimeFormatter.ofPattern("EEEE", Locale.ENGLISH)
        return LocalDate.parse(this, dtfInput).format(dtfOutput)
    }
}