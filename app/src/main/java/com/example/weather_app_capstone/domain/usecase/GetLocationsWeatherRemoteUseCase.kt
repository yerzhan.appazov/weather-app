package com.example.weather_app_capstone.domain.usecase

import com.example.weather_app_capstone.data.repository.WeatherRepository
import com.example.weather_app_capstone.domain.mapper.CurrentWeatherMapper
import com.example.weather_app_capstone.domain.mapper.LocationsWeatherMapper
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel
import javax.inject.Inject

class GetLocationsWeatherRemoteUseCase @Inject constructor(
    private val repository: WeatherRepository,
    private val locationsMapper: LocationsWeatherMapper,
    private val currentWeatherMapper: CurrentWeatherMapper
) {
    suspend fun execute(
        savedWeathers: List<SavedLocationsWeatherDomainModel>,
        token: String,
        units: String
    ): List<SavedLocationsWeatherDomainModel> {

        val weathersList = mutableListOf<SavedLocationsWeatherDomainModel>()
        savedWeathers.forEach {
            val currentWeatherFromApi =
                repository.getCurrentWeatherRemote(it.latitude, it.longitude, token, units)!!
            weathersList.add(
                locationsMapper.mapCurrentWeatherToLocations(
                    currentWeatherMapper.mapCurrentWeatherToDomainModel(currentWeatherFromApi)
                )
            )
        }
        return weathersList
    }
}