package com.example.weather_app_capstone.di

import android.app.Application
import android.content.Context
import com.example.weather_app_capstone.common.BASE_URL
import com.example.weather_app_capstone.data.datasource.local.WeatherDAO
import com.example.weather_app_capstone.data.datasource.local.WeatherDB
import com.example.weather_app_capstone.data.datasource.remote.WeatherDataSource
import com.example.weather_app_capstone.data.datasource.remote.WeatherDataSourceImplementation
import com.example.weather_app_capstone.data.datasource.remote.api.WeatherServiceApi
import com.example.weather_app_capstone.data.repository.WeatherRepository
import com.example.weather_app_capstone.data.repository.WeatherRepositoryImplementation
import com.example.weather_app_capstone.domain.mapper.LocationsWeatherMapper
import com.example.weather_app_capstone.domain.usecase.GetCurrentWeatherLocalUseCase
import com.example.weather_app_capstone.domain.usecase.GetCurrentWeatherRemoteUseCase
import com.example.weather_app_capstone.domain.usecase.GetDailyForecastLocalUseCase
import com.example.weather_app_capstone.domain.usecase.GetDailyForecastRemoteUseCase
import com.example.weather_app_capstone.domain.usecase.GetLocationsWeatherLocalUseCase
import com.example.weather_app_capstone.domain.usecase.GetLocationsWeatherRemoteUseCase
import com.example.weather_app_capstone.presentation.adapter.LocationListAdapter
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Singleton
@Component(modules = [
    RepositoryModule::class,
    DataModule::class,
    NetworkModule::class,
    AppModule::class
])

interface AppComponent {
    fun inject(listAdapter: LocationListAdapter)
    fun currentWeatherUseCase(): GetCurrentWeatherRemoteUseCase
    fun currentWeatherLocalUseCase(): GetCurrentWeatherLocalUseCase
    fun dailyForecastUseCase(): GetDailyForecastRemoteUseCase
    fun dailyForecastLocalUseCase(): GetDailyForecastLocalUseCase
    fun locationsWeatherUseCase(): GetLocationsWeatherRemoteUseCase
    fun locationsWeatherLocalUseCase(): GetLocationsWeatherLocalUseCase
    fun weatherDao(): WeatherDAO
    fun homeToLocationsMapper(): LocationsWeatherMapper
}

@Module
interface RepositoryModule {
    @Binds
    fun provideWeatherRepository(repository: WeatherRepositoryImplementation): WeatherRepository
}

@Module
interface DataModule {
    @Binds
    fun provideWeatherRemoteDataSource(dataSource: WeatherDataSourceImplementation): WeatherDataSource

}

@Module
object NetworkModule {

    @Provides
    fun provideWeatherDataApi(retrofit: Retrofit): WeatherServiceApi {
        return retrofit.create(WeatherServiceApi::class.java)
    }

    @Provides
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

}

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun providesApplicationContext(): Context = application

    @Provides
    @Singleton
    fun provideWeatherDao(): WeatherDAO = WeatherDB.getDatabase(application).weatherDao()
}

object DependencyManager {
    lateinit var appComponent: AppComponent
}
