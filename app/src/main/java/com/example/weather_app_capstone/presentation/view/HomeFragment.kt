package com.example.weather_app_capstone.presentation.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.example.weather_app_capstone.R
import com.example.weather_app_capstone.common.DAY_NIGHT_MODE
import com.example.weather_app_capstone.common.EMPTY_STRING
import com.example.weather_app_capstone.common.LATITUDE_BUNDLE_KEY
import com.example.weather_app_capstone.common.LONGITUDE_BUNDLE_KEY
import com.example.weather_app_capstone.common.NIGHT_KEY
import com.example.weather_app_capstone.databinding.FragmentHomeBinding
import com.example.weather_app_capstone.domain.model.DailyForecastDomainModel
import com.example.weather_app_capstone.domain.model.HomeWeatherDomainModel
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel
import com.example.weather_app_capstone.presentation.viewmodel.MainViewModel
import kotlin.math.roundToInt

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val mainViewModel by activityViewModels<MainViewModel>()
    private lateinit var sharedPreferences: SharedPreferences
    private var isNightModeEnabled: Boolean = false
    private var locationFromArgs = EMPTY_STRING to EMPTY_STRING

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            val lat = bundle.getString(LATITUDE_BUNDLE_KEY)
            val lng = bundle.getString(LONGITUDE_BUNDLE_KEY)
            if (lat == null || lng == null) {
                return
            }
            locationFromArgs = locationFromArgs.copy(lat, lng)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        checkSharedPrefs()
        setClickListeners()
        observeViewModel()
        return binding.root
    }

    private fun checkSharedPrefs() {
        sharedPreferences =
            requireContext().getSharedPreferences(DAY_NIGHT_MODE, Context.MODE_PRIVATE)
        isNightModeEnabled = sharedPreferences.getBoolean(NIGHT_KEY, false)
        if (isNightModeEnabled) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
    }

    private fun observeViewModel() {
            mainViewModel.getCurrentWeatherLocal()
            mainViewModel.getDailyForecastLocal()
            mainViewModel.getSavedLocationsWeather()
        if (locationFromArgs.first == EMPTY_STRING) {
            mainViewModel.location.observe(viewLifecycleOwner) {
                if (internetNotAvailable()) return@observe
                mainViewModel.getCurrentWeatherRemote()
                mainViewModel.getDailyForecastRemote()
            }
        } else {
            if (!internetNotAvailable()) {
                mainViewModel.getCurrentWeatherRemote(locationFromArgs)
                mainViewModel.getDailyForecastRemote(locationFromArgs)
            }
        }
            mainViewModel.currentWeatherModel.observe(viewLifecycleOwner) {
                setCurrentWeatherUI(it)
            }

            mainViewModel.dailyForecastWeatherModel.observe(viewLifecycleOwner) {
                if (it == null) {
                    return@observe
                }
                setDailyForecastUI(it)
            }
    }

    private fun internetNotAvailable(): Boolean {
        if (!((requireActivity() as MainActivity).isOnline())) {
            (requireActivity() as MainActivity).showSnackbar(resources.getString(R.string.internet_is_disabled))
            return true
        }
        return false
    }
    private fun setCurrentWeatherUI(it: HomeWeatherDomainModel?) {
        if (it == null) return
        binding.locationTV.text = it.name
        binding.degreesTV.text = resources.getString(R.string.degree, it.temp.roundToInt())
        binding.descriptionTV.text = it.description
        binding.highestLowestTv.text = resources.getString(
            R.string.highest_lowest,
            it.highest.roundToInt(),
            it.lowest.roundToInt()
        )

        if (mainViewModel.locationsWeatherModel.value?.contains(it) == true) {
            binding.saveLocationIV.setImageResource(R.drawable.heart_fill_red)
        } else {
            if (isNightModeEnabled) binding.saveLocationIV.setImageResource(R.drawable.heart_white)
            else binding.saveLocationIV.setImageResource(R.drawable.heart_black_corner)
        }
    }

    private fun setDailyForecastUI(listOfDailyForecast: List<DailyForecastDomainModel>) {
        binding.firstWeatherForecastTv.text = resources.getString(
            R.string.first_forecast_text,
            listOfDailyForecast[0].temp.roundToInt(),
            listOfDailyForecast[0].description
        )
        binding.secondWeatherForecastTv.text = resources.getString(
            R.string.second_forecast_text,
            listOfDailyForecast[1].temp.roundToInt(),
            listOfDailyForecast[1].description
        )
        binding.thirdWeatherForecastTv.text = resources.getString(
            R.string.third_forecast_text,
            listOfDailyForecast[2].time,
            listOfDailyForecast[2].temp.roundToInt(),
            listOfDailyForecast[2].description
        )
        binding.fourthWeatherForecastTv.text = resources.getString(
            R.string.fourth_forecast_text,
            listOfDailyForecast[3].time,
            listOfDailyForecast[3].temp.roundToInt(),
            listOfDailyForecast[3].description
        )
        binding.fifthWeatherForecastTv.text = resources.getString(
            R.string.fifth_forecast_text,
            listOfDailyForecast[4].time,
            listOfDailyForecast[4].temp.roundToInt(),
            listOfDailyForecast[4].description
        )

        Glide.with(binding.root).load(listOfDailyForecast[0].ico).into(binding.firstForecastImage)
        Glide.with(binding.root).load(listOfDailyForecast[1].ico).into(binding.secondForecastImage)
        Glide.with(binding.root).load(listOfDailyForecast[2].ico).into(binding.thirdForecastImage)
        Glide.with(binding.root).load(listOfDailyForecast[3].ico).into(binding.fourthForecastImage)
        Glide.with(binding.root).load(listOfDailyForecast[4].ico).into(binding.fifthForecastImage)
    }

    private fun setClickListeners() {
        binding.setDayNight.setOnClickListener {
            val editor = sharedPreferences.edit()
            val isNightModeEnabled = sharedPreferences.getBoolean(NIGHT_KEY, false)
            if (isNightModeEnabled) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                editor.putBoolean(NIGHT_KEY, false)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                editor.putBoolean(NIGHT_KEY, true)
            }
            editor.apply()
        }

        binding.saveLocationIV.setOnClickListener {
            if (mainViewModel.locationsWeatherModel.value.contains(mainViewModel.currentWeatherModel.value!!)) {
                if (isNightModeEnabled) {
                    it.setBackgroundResource(R.drawable.heart_white)
                } else {
                    it.setBackgroundResource(R.drawable.heart_black_corner)
                }
                mainViewModel.deleteLocation()
            } else {
                it.setBackgroundResource(R.drawable.heart_fill_red)
                mainViewModel.saveLocation()
            }
        }
    }

    private fun List<SavedLocationsWeatherDomainModel>?.contains(homeWeather: HomeWeatherDomainModel): Boolean {
        if (this == null) {
            return false
        }
        for (location in this) {
            if (location.cityId == homeWeather.cityId) {
                return true
            }
        }
        return false
    }

}