package com.example.weather_app_capstone.presentation.model

data class LocationCategoryUIModel(val categoryName: String)