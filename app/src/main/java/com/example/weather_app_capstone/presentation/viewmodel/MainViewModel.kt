package com.example.weather_app_capstone.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.weather_app_capstone.common.METRIC_UNIT
import com.example.weather_app_capstone.common.TOKEN
import com.example.weather_app_capstone.data.datasource.local.WeatherDAO
import com.example.weather_app_capstone.di.DependencyManager
import com.example.weather_app_capstone.domain.mapper.LocationsWeatherMapper
import com.example.weather_app_capstone.domain.model.HomeWeatherDomainModel
import com.example.weather_app_capstone.domain.model.DailyForecastDomainModel
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel
import com.example.weather_app_capstone.domain.usecase.GetCurrentWeatherLocalUseCase
import com.example.weather_app_capstone.domain.usecase.GetCurrentWeatherRemoteUseCase
import com.example.weather_app_capstone.domain.usecase.GetDailyForecastLocalUseCase
import com.example.weather_app_capstone.domain.usecase.GetDailyForecastRemoteUseCase
import com.example.weather_app_capstone.domain.usecase.GetLocationsWeatherLocalUseCase
import com.example.weather_app_capstone.domain.usecase.GetLocationsWeatherRemoteUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val getCurrentWeatherRemoteUseCase: GetCurrentWeatherRemoteUseCase,
    private val getCurrentWeatherLocalUseCase: GetCurrentWeatherLocalUseCase,
    private val getDailyForecastRemoteUseCase: GetDailyForecastRemoteUseCase,
    private val getDailyForecastLocalUseCase: GetDailyForecastLocalUseCase,
    private val getLocationsWeatherRemoteUseCase: GetLocationsWeatherRemoteUseCase,
    private val getLocationsWeatherLocalUseCase: GetLocationsWeatherLocalUseCase,
    private val weatherDao: WeatherDAO,
    private val homeToLocationsMapper: LocationsWeatherMapper
) : ViewModel() {


    private val _currentWeatherModel: MutableLiveData<HomeWeatherDomainModel> = MutableLiveData()
    val currentWeatherModel: LiveData<HomeWeatherDomainModel> = _currentWeatherModel

    private val _dailyForecastWeatherModel: MutableLiveData<List<DailyForecastDomainModel>> =
        MutableLiveData()
    val dailyForecastWeatherModel: LiveData<List<DailyForecastDomainModel>> =
        _dailyForecastWeatherModel

    private val _locationsWeatherModel: MutableLiveData<List<SavedLocationsWeatherDomainModel>> =
        MutableLiveData()
    val locationsWeatherModel: LiveData<List<SavedLocationsWeatherDomainModel>> =
        _locationsWeatherModel

    private val _errorToSnackBar: MutableLiveData<String> = MutableLiveData()
    val errorToSnackBar: LiveData<String> = _errorToSnackBar

    private val _location: MutableLiveData<Pair<Double, Double>> = MutableLiveData()
    val location: LiveData<Pair<Double, Double>> = _location

    fun getCurrentWeatherLocal() {
        CoroutineScope(Dispatchers.IO).launch {
            _currentWeatherModel.postValue(getCurrentWeatherLocalUseCase.execute())
        }
    }

    fun getCurrentWeatherRemote() {
        CoroutineScope(Dispatchers.IO).launch {
            _currentWeatherModel.postValue(
                getCurrentWeatherRemoteUseCase.execute(
                    location.value?.first.toString(),
                    location.value?.second.toString(),
                    TOKEN,
                    METRIC_UNIT
                )
            )
            currentWeatherModel.value?.let {
                Log.d("TAG", "insertingToDatabase: $it")
                weatherDao.insertHomeWeather(it)
            }
        }
    }

    fun getCurrentWeatherRemote(locationParameter: Pair<String, String>) {
        CoroutineScope(Dispatchers.IO).launch {
            _currentWeatherModel.postValue(
                getCurrentWeatherRemoteUseCase.execute(
                    locationParameter.first,
                    locationParameter.second,
                    TOKEN,
                    METRIC_UNIT
                )
            )
            currentWeatherModel.value?.let {
                Log.d("TAG", "insertingToDatabase: $it")
                weatherDao.insertHomeWeather(it)
            }
        }
    }

    fun getDailyForecastLocal() {
        CoroutineScope(Dispatchers.IO).launch {
            val localData = getDailyForecastLocalUseCase.execute()
            if (localData.isNotEmpty())
                _dailyForecastWeatherModel.postValue(localData)
        }
    }

    fun getDailyForecastRemote() {
        CoroutineScope(Dispatchers.IO).launch {
            _dailyForecastWeatherModel.postValue(
                getDailyForecastRemoteUseCase.execute(
                    location.value?.first.toString(),
                    location.value?.second.toString(),
                    TOKEN,
                    METRIC_UNIT
                )
            )
            dailyForecastWeatherModel.value?.let {
                weatherDao.apply {
                    clearDailyForecastTable()
                    insertDailyForecast(it)
                }
            }
        }
    }

    fun getDailyForecastRemote(locationParameter: Pair<String, String>) {
        CoroutineScope(Dispatchers.IO).launch {
            _dailyForecastWeatherModel.postValue(
                getDailyForecastRemoteUseCase.execute(
                    locationParameter.first,
                    locationParameter.second,
                    TOKEN,
                    METRIC_UNIT
                )
            )
            dailyForecastWeatherModel.value?.let {
                weatherDao.apply {
                    clearDailyForecastTable()
                    insertDailyForecast(it)
                }
            }
        }
    }

    fun getSavedLocationsWeather() {
        CoroutineScope(Dispatchers.IO).launch {
            _locationsWeatherModel.postValue(getLocationsWeatherLocalUseCase.execute())
        }
    }

    fun getLocationsWeatherRemote() {
        CoroutineScope(Dispatchers.IO).launch {
            _locationsWeatherModel.postValue(
                locationsWeatherModel.value?.let {
                    getLocationsWeatherRemoteUseCase.execute(
                        it,
                        TOKEN,
                        METRIC_UNIT
                    )
                }
            )
        }
    }

    fun setLocation(latitude: Double, longitude: Double) {
        _location.value = latitude to longitude
        Log.d("TAG", "setLocation: ${_location.value}")
    }

    fun saveLocation() {
        viewModelScope.launch {
            val savedLocations = weatherDao.getSavedLocations() as ArrayList
            currentWeatherModel.value?.let {
                savedLocations.add(homeToLocationsMapper.mapCurrentWeatherToLocations(it))
                weatherDao.insertSavedLocation(homeToLocationsMapper.mapCurrentWeatherToLocations(it))
            }
            _currentWeatherModel.postValue(currentWeatherModel.value)
            _locationsWeatherModel.postValue(savedLocations)
        }
    }

    fun deleteLocation() {
        viewModelScope.launch {
            currentWeatherModel.value?.let {
                weatherDao.deleteSavedLocation(homeToLocationsMapper.mapCurrentWeatherToLocations(it))
                _locationsWeatherModel.postValue(weatherDao.getSavedLocations())
                _currentWeatherModel.postValue(currentWeatherModel.value)
            }
        }
    }

    companion object {
        fun factory() = viewModelFactory {
            val component = DependencyManager.appComponent
            initializer {
                MainViewModel(
                    component.currentWeatherUseCase(),
                    component.currentWeatherLocalUseCase(),
                    component.dailyForecastUseCase(),
                    component.dailyForecastLocalUseCase(),
                    component.locationsWeatherUseCase(),
                    component.locationsWeatherLocalUseCase(),
                    component.weatherDao(),
                    component.homeToLocationsMapper()
                )
            }
        }
    }
}
