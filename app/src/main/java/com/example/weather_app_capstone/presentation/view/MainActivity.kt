package com.example.weather_app_capstone.presentation.view

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.weather_app_capstone.R
import com.example.weather_app_capstone.common.MIN_DISTANCE_OF_LOCATION_UPDATE
import com.example.weather_app_capstone.common.TIME_INTERVAL_BETWEEN_LOCATION_UPDATES
import com.example.weather_app_capstone.databinding.ActivityMainBinding
import com.example.weather_app_capstone.presentation.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity(), LocationListener {
    private lateinit var binding: ActivityMainBinding
    private lateinit var locationManager: LocationManager
    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                Log.i(TAG, resources.getString(R.string.permission_granted))
            } else {
                Log.i(TAG, resources.getString(R.string.permission_denied))
                Toast.makeText(
                    this,
                    resources.getString(R.string.permission_needed),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    val viewModel by viewModels<MainViewModel>(
        factoryProducer = { MainViewModel.factory() }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        setContentView(binding.root)
        setClickListeners()

        requestPermission()
        observeViewModel()


    }

    private fun requestPermission() {
        when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED -> {
                Log.i(TAG, resources.getString(R.string.permission_granted))
                locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    TIME_INTERVAL_BETWEEN_LOCATION_UPDATES,
                    MIN_DISTANCE_OF_LOCATION_UPDATE,
                    this
                )
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) -> {
                Toast.makeText(
                    this,
                    resources.getString(R.string.permission_needed),
                    Toast.LENGTH_SHORT
                ).show()
                requestPermissionLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
            }

            else -> {
                requestPermissionLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
            }
        }
    }

    override fun onLocationChanged(location: Location) {
        viewModel.setLocation(location.latitude, location.longitude)
    }

    private fun observeViewModel() {
        viewModel.errorToSnackBar.observe(this) {}
    }


    private fun setClickListeners() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        binding.bottomNavigationView.setupWithNavController(navController)

    }

    fun isOnline(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                return true
            }
        }
        return false
    }

    fun showSnackbar(text: String) {
        val snackbar = Snackbar.make(binding.root, text, Snackbar.LENGTH_SHORT)
        snackbar.apply {
            setAction(resources.getString(R.string.dismiss)) { snackbar.dismiss() }
            setTextColor(resources.getColor(R.color.snackbar_text, resources.newTheme()))
            setBackgroundTint(resources.getColor(R.color.snackbar_background, resources.newTheme()))
            setActionTextColor(resources.getColor(R.color.snackbar_text, resources.newTheme()))
        }
        snackbar.show()
    }

}