package com.example.weather_app_capstone.presentation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weather_app_capstone.R
import com.example.weather_app_capstone.databinding.FragmentLocationsBinding
import com.example.weather_app_capstone.presentation.adapter.LocationListAdapter
import com.example.weather_app_capstone.presentation.viewmodel.MainViewModel

class LocationsFragment : Fragment() {
    private lateinit var binding: FragmentLocationsBinding
    private lateinit var locationsAdapter: LocationListAdapter
    private val mainViewModel by activityViewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLocationsBinding.inflate(inflater, container, false)
        setRecyclerView()
        observeViewModel()
        return binding.root
    }

    private fun observeViewModel() {
        mainViewModel.getSavedLocationsWeather()
        if (!((requireActivity() as MainActivity).isOnline())) {
            (requireActivity() as MainActivity).showSnackbar(resources.getString(R.string.internet_is_disabled))
        } else {
            mainViewModel.getLocationsWeatherRemote()
        }
        mainViewModel.locationsWeatherModel.observe(viewLifecycleOwner) {
            locationsAdapter.setList(it)
        }
    }

    private fun setRecyclerView() {
        locationsAdapter = LocationListAdapter(emptyList(), findNavController())
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = locationsAdapter
        }
    }
}