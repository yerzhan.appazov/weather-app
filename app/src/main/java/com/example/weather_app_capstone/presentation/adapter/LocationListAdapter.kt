package com.example.weather_app_capstone.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.weather_app_capstone.R
import com.example.weather_app_capstone.common.LATITUDE_BUNDLE_KEY
import com.example.weather_app_capstone.common.LONGITUDE_BUNDLE_KEY
import com.example.weather_app_capstone.common.MAIN_CATEGORY
import com.example.weather_app_capstone.common.OTHERS_CATEGORY
import com.example.weather_app_capstone.databinding.LocationCategoryLayoutBinding
import com.example.weather_app_capstone.databinding.LocationItemLayoutBinding
import com.example.weather_app_capstone.di.DependencyManager
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel
import com.example.weather_app_capstone.presentation.model.LocationCategoryUIModel
import javax.inject.Inject
import kotlin.math.roundToInt

class LocationListAdapter @Inject constructor(
    private var listOfItems: List<Any>,
    private val navController: NavController
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        DependencyManager.appComponent.inject(this)
        return when (viewType) {
            CATEGORY_ITEM_VIEW -> CategoryViewHolder(
                LocationCategoryLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            LOCATION_ITEM_VIEW -> LocationViewHolder(
                LocationItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            else -> error(parent.context.getString(R.string.recycler_view_invalid_viewtype_error))
        }
    }

    override fun getItemCount(): Int {
        return listOfItems.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LocationViewHolder -> {
                val currentWeatherLocation = listOfItems[position]
                if (currentWeatherLocation is SavedLocationsWeatherDomainModel) {
                    holder.degrees.text = holder.itemView.context.getString(
                        R.string.degree,
                        currentWeatherLocation.temp.roundToInt()
                    )
                    holder.description.text = currentWeatherLocation.description
                    holder.location.text = currentWeatherLocation.name
                    holder.icon.setImageResource(R.drawable.cloud_rain_icon)
                    holder.highestLowest.text = holder.itemView.context.getString(
                        R.string.highest_lowest,
                        currentWeatherLocation.highest.roundToInt(),
                        currentWeatherLocation.lowest.roundToInt()
                    )
                    Glide.with(holder.itemView)
                        .load(currentWeatherLocation.icon)
                        .into(holder.icon)
                    holder.itemView.setOnClickListener {
                        val bundle = bundleOf(
                            LATITUDE_BUNDLE_KEY to currentWeatherLocation.latitude,
                            LONGITUDE_BUNDLE_KEY to currentWeatherLocation.longitude
                        )
                        navController.navigate(R.id.action_locationsFragment_to_homeFragment, bundle)
                    }
                }

            }
            is CategoryViewHolder -> {
                val currentCategoryItem = listOfItems[position]
                if (currentCategoryItem is LocationCategoryUIModel) {
                    holder.categoryName.text = currentCategoryItem.categoryName
                }
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setList(list: List<Any>) {
        val updatedList = arrayListOf<Any>(LocationCategoryUIModel(MAIN_CATEGORY))
        updatedList.addAll(list)
        updatedList.add(LocationCategoryUIModel(OTHERS_CATEGORY))
        listOfItems = updatedList
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when(listOfItems[position]) {
            is LocationCategoryUIModel -> CATEGORY_ITEM_VIEW
            else -> LOCATION_ITEM_VIEW
        }
    }

    inner class LocationViewHolder(binding: LocationItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val location: TextView = binding.locationsLocationTv
        val degrees: TextView = binding.locationsDegreesTv
        val icon: ImageView = binding.locationsWeatherIcon
        val description: TextView = binding.locationsDescriptionTv
        val highestLowest: TextView = binding.locationsHighestLowestTv
    }

    inner class CategoryViewHolder(binding: LocationCategoryLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
            val categoryName: TextView = binding.categoryNameTv
        }

    companion object {
        const val CATEGORY_ITEM_VIEW = 1
        const val LOCATION_ITEM_VIEW = 2
    }
}