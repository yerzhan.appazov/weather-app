package com.example.weather_app_capstone.data.datasource.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.weather_app_capstone.common.DATABASE_PHYSICAL_NAME
import com.example.weather_app_capstone.domain.model.HomeWeatherDomainModel
import com.example.weather_app_capstone.domain.model.DailyForecastDomainModel
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel

@Database(
    entities = [
        HomeWeatherDomainModel::class,
        DailyForecastDomainModel::class,
        SavedLocationsWeatherDomainModel::class],
    version = 5,
    exportSchema = false
)

abstract class WeatherDB : RoomDatabase() {

    abstract fun weatherDao(): WeatherDAO

    companion object {
        @Volatile
        private var INSTANCE: WeatherDB? = null

        fun getDatabase(context: Context): WeatherDB {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        WeatherDB::class.java,
                        DATABASE_PHYSICAL_NAME
                    ).fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE!!
        }
    }
}