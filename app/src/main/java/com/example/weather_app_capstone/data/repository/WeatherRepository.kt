package com.example.weather_app_capstone.data.repository

import com.example.weather_app_capstone.data.model.CurrentWeatherDataModel
import com.example.weather_app_capstone.data.model.DailyForecastDataModel
import com.example.weather_app_capstone.domain.model.HomeWeatherDomainModel
import com.example.weather_app_capstone.domain.model.DailyForecastDomainModel
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel

interface WeatherRepository {
    suspend fun getCurrentWeatherRemote(latitude: String, longitude: String, token: String, units: String): CurrentWeatherDataModel?
    suspend fun getDailyForecastRemote(latitude: String, longitude: String, token: String, units: String): DailyForecastDataModel?
    suspend fun getHomeWeatherLocal(): HomeWeatherDomainModel
    suspend fun getDailyForecastLocal(): List<DailyForecastDomainModel>
    suspend fun getSavedLocationsLocal(): List<SavedLocationsWeatherDomainModel>?
}