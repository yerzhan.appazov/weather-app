package com.example.weather_app_capstone.data.repository

import android.util.Log
import com.example.weather_app_capstone.data.datasource.local.WeatherDAO
import com.example.weather_app_capstone.data.datasource.remote.WeatherDataSource
import com.example.weather_app_capstone.data.model.CurrentWeatherDataModel
import com.example.weather_app_capstone.data.model.DailyForecastDataModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

class WeatherRepositoryImplementation @Inject constructor(
    private val remoteDataSource: WeatherDataSource,
    private val localDataSource: WeatherDAO
    ) :
    WeatherRepository {
    override suspend fun getCurrentWeatherRemote(
        latitude: String,
        longitude: String,
        token: String,
        units: String
    ): CurrentWeatherDataModel? {
        var currentWeatherDataModel: CurrentWeatherDataModel? = null
        coroutineScope {
            launch {
                remoteDataSource.getCurrentWeather(latitude, longitude, token, units)
                    .flowOn(Dispatchers.IO)
                    .catch { e -> error(e) }
                    .collect {
                        currentWeatherDataModel = it
                    }
            }
        }

        return currentWeatherDataModel
    }

    override suspend fun getDailyForecastRemote(
        latitude: String,
        longitude: String,
        token: String,
        units: String
    ): DailyForecastDataModel? {
        var dailyForecastDataModel: DailyForecastDataModel? = null
        coroutineScope {
            launch {
                remoteDataSource.getDailyForecast(latitude, longitude, token, units)
                    .flowOn(Dispatchers.IO)
                    .catch { e -> error(e) }
                    .collect {
                        dailyForecastDataModel = it
                    }
            }
        }
        return dailyForecastDataModel
    }

    override suspend fun getHomeWeatherLocal() = localDataSource.getHomeWeather()
    override suspend fun getSavedLocationsLocal() = localDataSource.getSavedLocations()
    override suspend fun getDailyForecastLocal() = localDataSource.getDailyForecast()

}