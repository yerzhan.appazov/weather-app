package com.example.weather_app_capstone.data.datasource.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.weather_app_capstone.domain.model.HomeWeatherDomainModel
import com.example.weather_app_capstone.domain.model.DailyForecastDomainModel
import com.example.weather_app_capstone.domain.model.SavedLocationsWeatherDomainModel

@Dao
interface WeatherDAO {

    @Query("SELECT * FROM current_weather ORDER BY cityId DESC LIMIT 1")
    suspend fun getHomeWeather(): HomeWeatherDomainModel

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHomeWeather(weather: HomeWeatherDomainModel)

    @Query("SELECT * FROM daily_forecast")
    suspend fun getDailyForecast(): List<DailyForecastDomainModel>

    @Query("DELETE FROM daily_forecast")
    suspend fun clearDailyForecastTable()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDailyForecast(dailyForecast: List<DailyForecastDomainModel>)

    @Query("SELECT * FROM saved_locations ORDER BY cityId")
    suspend fun getSavedLocations(): List<SavedLocationsWeatherDomainModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSavedLocation(weather: SavedLocationsWeatherDomainModel)

    @Delete
    suspend fun deleteSavedLocation(weather: SavedLocationsWeatherDomainModel)

}