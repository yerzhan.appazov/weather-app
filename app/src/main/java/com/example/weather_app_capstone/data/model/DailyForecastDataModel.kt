package com.example.weather_app_capstone.data.model

data class DailyForecastDataModel(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: List<Cnt>,
    val message: Int,
)

data class City(
    val coord: Coord,
    val country: String,
    val id: Int,
    val name: String,
    val population: Int,
    val sunrise: Int,
    val sunset: Int,
    val timezone: Int
)

data class Cnt(
    val clouds: Clouds,
    val dt: Int,
    val dt_txt: String,
    val main: Main2,
    val pop: Double,
    val rain: Rain,
    val sys: Sys2,
    val visibility: Int,
    val weather: List<Weather>,
    val wind: Wind
)

data class Main2(
    val feels_like: Double,
    val grnd_level: Int,
    val humidity: Int,
    val pressure: Int,
    val sea_level: Int,
    val temp: Double,
    val temp_kf: Double,
    val temp_max: Double,
    val temp_min: Double
)

data class Rain(
    val `3h`: Double
)

data class Sys2(
    val pod: String
)

data class Wind(
    val deg: Int,
    val gust: Double,
    val speed: Double
)