package com.example.weather_app_capstone.data.datasource.remote

import com.example.weather_app_capstone.data.datasource.remote.api.WeatherServiceApi
import com.example.weather_app_capstone.data.model.DailyForecastDataModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class WeatherDataSourceImplementation @Inject constructor(private val api: WeatherServiceApi) :
    WeatherDataSource {
    override suspend fun getCurrentWeather(
        latitude: String,
        longitude: String,
        token: String,
        units: String
    ) = flow { emit(api.getCurrentWeather(latitude, longitude, token, units)) }

    override suspend fun getDailyForecast(
        latitude: String,
        longitude: String,
        token: String,
        units: String
    ): Flow<DailyForecastDataModel> =
        flow { emit(api.getDailyForecast(latitude, longitude, token, units)) }
}