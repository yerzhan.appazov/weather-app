package com.example.weather_app_capstone.data.datasource.remote.api

import com.example.weather_app_capstone.data.model.CurrentWeatherDataModel
import com.example.weather_app_capstone.data.model.DailyForecastDataModel
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherServiceApi {
    @GET("weather")
    suspend fun getCurrentWeather(
        @Query("lat") latitude: String,
        @Query("lon") longitude: String,
        @Query("appid") token: String,
        @Query("units") units: String
    ) : CurrentWeatherDataModel

    @GET("forecast")
    suspend fun getDailyForecast(
        @Query("lat") latitude: String,
        @Query("lon") longitude: String,
        @Query("appid") token: String,
        @Query("units") units: String
    ) : DailyForecastDataModel
}