package com.example.weather_app_capstone.data.datasource.remote

import com.example.weather_app_capstone.data.model.CurrentWeatherDataModel
import com.example.weather_app_capstone.data.model.DailyForecastDataModel
import kotlinx.coroutines.flow.Flow

interface WeatherDataSource {
    suspend fun getCurrentWeather(latitude: String, longitude: String, token: String, units: String): Flow<CurrentWeatherDataModel>

    suspend fun getDailyForecast(latitude: String, longitude: String, token: String, units: String): Flow<DailyForecastDataModel>
}