package com.example.weather_app_capstone

import android.app.Application
import com.example.weather_app_capstone.di.AppModule
import com.example.weather_app_capstone.di.DaggerAppComponent
import com.example.weather_app_capstone.di.DependencyManager

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        DependencyManager.appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }
}