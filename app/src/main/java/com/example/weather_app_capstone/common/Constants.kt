package com.example.weather_app_capstone.common

const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
const val ICON_BASE_URL = "https://openweathermap.org/img/w/"
const val TOKEN = "d7a0ecdd370ec253375f726b852968ec"
const val METRIC_UNIT = "metric"
const val DATABASE_PHYSICAL_NAME = "weather.db"
const val DAY_NIGHT_MODE = "mode"
const val NIGHT_KEY = "night_mode_enabled"
const val LATITUDE_BUNDLE_KEY = "latitude"
const val LONGITUDE_BUNDLE_KEY = "longitude"
const val EMPTY_STRING = ""
const val MAIN_CATEGORY = "Main"
const val OTHERS_CATEGORY = "Others"
const val TIME_AFTER_NOON = "15:00:00"
const val DAILY_FORECAST_TABLE = "daily_forecast"
const val CURRENT_WEATHER_TABLE = "current_weather"
const val SAVED_LOCATIONS_TABLE = "saved_locations"
const val TIME_INTERVAL_BETWEEN_LOCATION_UPDATES: Long = 5000
const val MIN_DISTANCE_OF_LOCATION_UPDATE = 5f





